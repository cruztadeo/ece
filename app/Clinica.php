<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    public function administrador(){
        return $this->hasOne('App\Administrador');
    }
    public function doctor(){
        return $this->belongsTo('App\Doctor');
    }
}
