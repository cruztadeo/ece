<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('Admin')->prefix('administrators')->name('administrators.')->middleware('can:manage-users')->group(function (
) {
    Route::resource('/users', 'UsersController', ['except' => ['show', 'create', 'store']]);
});

Route::post('/doctor/register', 'DoctorController@register')->name('register');
Route::get('/doctor/registro', 'DoctorController@registro')->name('registro');

route::get('/admins/login', 'AdminController@showLoginForm');
route::post('/admins/login', 'AdminController@login');

Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function () {
    route::get('/area', function (){
        return view('welcome')->with(array(
            'message'=>"hola bbs"
        ));
    });
});


